package alien.site;

/**
 * @author Joern-Are Flaten
 */
public class SiteStatusDTO {

	private String siteId;
	private String statusId;
	private long count;
	private long totalCost;

	@SuppressWarnings("javadoc")
	public SiteStatusDTO(String siteId, String statusId, long count, long totalCost) {
		this.siteId = siteId;
		this.statusId = statusId;
		this.count = count;
		this.totalCost = totalCost;
	}

	@SuppressWarnings("javadoc")
	public SiteStatusDTO() {
	}

	@SuppressWarnings("javadoc")
	public String getSiteId() {
		return siteId;
	}

	@SuppressWarnings("javadoc")
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	@SuppressWarnings("javadoc")
	public String getStatusId() {
		return statusId;
	}

	@SuppressWarnings("javadoc")
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	@SuppressWarnings("javadoc")
	public long getCount() {
		return count;
	}

	@SuppressWarnings("javadoc")
	public void setCount(long count) {
		this.count = count;
	}

	@SuppressWarnings("javadoc")
	public long getTotalCost() {
		return totalCost;
	}

	@SuppressWarnings("javadoc")
	public void setTotalCost(long totalCost) {
		this.totalCost = totalCost;
	}

	@Override
	public String toString() {
		return "SiteStatusDTO{" +
				"siteId='" + siteId + '\'' +
				", statusId='" + statusId + '\'' +
				", count=" + count +
				", totalCost=" + totalCost +
				'}';
	}
}
