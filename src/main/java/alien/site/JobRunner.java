package alien.site;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import alien.api.DispatchSSLClient;
import alien.config.ConfigUtils;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.shell.commands.JAliEnCOMMander;
import apmon.ApMon;
/**
 * @author sweisz
 * @since Mar 25, 2021
 */
public class JobRunner extends JobAgent {

	/**
	 * logger object
	 */
	private static final Logger logger = ConfigUtils.getLogger(JobRunner.class.getCanonicalName());

	/**
	 * ML monitor object
	 */
	static final Monitor monitor = MonitorFactory.getMonitor(JobRunner.class.getCanonicalName());

	static {
		monitor.addMonitoring("resource_status", (names, values) -> {
			names.add("totalcpu");
			values.add(JobAgent.MAX_CPU);

			names.add("availablecpu");
			values.add(JobAgent.RUNNING_CPU);

			names.add("allocatedcpu");
			values.add(Long.valueOf(JobAgent.MAX_CPU.longValue() - JobAgent.RUNNING_CPU.longValue()));

			names.add("runningja");
			values.add(Long.valueOf(JobAgent.RUNNING_JOBAGENTS));

			names.add("slotlength");
			values.add(Integer.valueOf(JobAgent.origTtl));

			names.add("totaloversubscribed");
			values.add(JobAgent.MAX_OVERSUBSCRIBED);

			names.add("allocatedoversubscribedcpu");
			values.add(Long.valueOf(JobAgent.MAX_OVERSUBSCRIBED.longValue() - JobAgent.OVERSUBSCRIBED_CPU.longValue()));
		});
	}

	boolean alreadyIsol = false;

	static Stack<JobAgent> oversubscribedJobs;

	@Override
	public void run() {
		long timestamp = System.currentTimeMillis() / 1000, lastLaunchTimestamp = System.currentTimeMillis() / 1000 - (4 * 60);
		final long ttlEnd = timestamp + JobAgent.origTtl;
		int i = 0;

		long idleTimestamp = 0, overloadedTimestamp = 0;
		oversubscribedJobs = new Stack<>();

		final int maxRetries = Integer.parseInt(System.getenv().getOrDefault("MAX_RETRIES", "2"));

		double memoryPerCore = 0, swapPerCore = 0;
		boolean enoughMem = false;
		JobAgent.OVERSUBSCRIBED_CPU = Long.valueOf(0);
		JobAgent.MAX_OVERSUBSCRIBED = Long.valueOf(0);

		long WAIT_OVERSUBSCRIBE = 240; //We will wait at least this time for starting a new oversubscribed job
		long OVERLOADED_MAXTIME = 120; //We tolerate 2 min of high CPU pressure
		long IDLE_CPU_LACK = 200; //If we only have 2 cores available --> abort
		long IDLE_CPU_OVERSUBSCRIBE = 300; //Free cores to start oversubscription workflow

		if (cpuOversubscription) {
			try {
				memoryPerCore = (MonitorFactory.getApMonSender().getSystemParameter("mem_free").doubleValue() + MonitorFactory.getApMonSender().getSystemParameter("mem_used").doubleValue()) / MAX_CPU.doubleValue();
				swapPerCore = (MonitorFactory.getApMonSender().getSystemParameter("swap_free").doubleValue() + MonitorFactory.getApMonSender().getSystemParameter("swap_used").doubleValue()) / MAX_CPU.doubleValue();
				logger.log(Level.INFO, "Memory per core " + memoryPerCore);
				logger.log(Level.INFO, "SWAP per core " + swapPerCore);

				enoughMem = memoryPerCore > 2 * 1024;
				logger.log(Level.INFO, "Oversubscription - By memory would add " + Math.floor(memoryPerCore/(2*1024)) + " cores");
				//logger.log(Level.INFO, "Oversubscription - By swap would add " + Math.floor(swapPerCore/(8*1024)) + " cores");

				//JobAgent.OVERSUBSCRIBED_CPU = Long.valueOf(Math.min((long)Math.floor(memoryPerCore/(2*1024)), (long)Math.floor(swapPerCore/(8*1024))));
				JobAgent.OVERSUBSCRIBED_CPU = Long.valueOf((long)Math.floor(memoryPerCore/(2*1024)));
				JobAgent.MAX_OVERSUBSCRIBED = JobAgent.OVERSUBSCRIBED_CPU;
				logger.log(Level.INFO, "Oversubscription - OVERSUBSCRIBED_CPU set to  " + JobAgent.OVERSUBSCRIBED_CPU);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Could not get the node's memory to set up the oversubscription pool", e);
				cpuOversubscription = false;
			}
		}
		double cpuIdle = 0;

		int jrPid = MonitorFactory.getSelfProcessID();

		try {
			CgroupUtils.setupTopCgroups(jrPid);
		}
		catch (Exception e) {
			logger.log(Level.WARNING, "Error creating top cgroup: ", e);
		}

		boolean JRNotified = false;
		while (timestamp < ttlEnd) {
			synchronized (JobAgent.requestSync) {
				try {
					try {
						if (cpuOversubscription) {
							ApMon.updateCPUComponents();
							try {
								cpuIdle = Double.parseDouble(ApMon.getIdleCPU());
							} catch (NumberFormatException nfe) {
								logger.log(Level.SEVERE, "Could not fetch machine's idle CPU", nfe);
								cpuIdle = 0;
							}
							if (JobAgent.OVERSUBSCRIBED_CPU.longValue() < JobAgent.MAX_OVERSUBSCRIBED.longValue()) {
								//Checking if it might be impacting other jobs
								if (cpuIdle * JobAgent.MAX_CPU.longValue() < IDLE_CPU_LACK) {
									logger.log(Level.INFO, "We lack idle. Current idle = " + cpuIdle + "%");
									if (overloadedTimestamp == 0)
										overloadedTimestamp = System.currentTimeMillis()/1000;
									logger.log(Level.INFO, "In overloaded for " + (timestamp - overloadedTimestamp) + " seconds");
									if (overloadedTimestamp != 0 && (timestamp - overloadedTimestamp) >= OVERLOADED_MAXTIME && !oversubscribedJobs.empty()) {
										JobAgent toKill = oversubscribedJobs.pop();
										String status = toKill.getWrapperJobStatus();
										if ("RUNNING".equals(status) || "SAVING".equals(status)) {
											logger.log(Level.INFO, "Oversubscription - Time to kill. process " + toKill.getQueueId());
											toKill.killForcibly(toKill.getJobWrapperProcess());
											logger.log(Level.INFO, "Oversubscription -  Going to change job status back to WAITING");
											putJobTrace("Job kill on oversubscription regime. Putting back to WAITING.");
											overloadedTimestamp = 0;

											WAIT_OVERSUBSCRIBE += 10 * 60; //Prevent black-holes
										}
									}
								} else if (cpuIdle * JobAgent.MAX_CPU.longValue() > IDLE_CPU_LACK && overloadedTimestamp != 0) {
										overloadedTimestamp = 0;
										logger.log(Level.INFO, "Reseting overloaded counter. CPU idle = " + cpuIdle + "%");
								}
							}
						}

						long elapsedTime = 0;
						if ((ttlEnd - timestamp) < 60 * 60 * 12) {
							elapsedTime = (long) (-0.086 * (ttlEnd - timestamp) + 3910); // until last 12h -> 3'. Then start decreasing until at the last 1h -> 60'
						} else {
							elapsedTime = 3l * 60;
						}
						if (JRNotified || (timestamp - lastLaunchTimestamp) > elapsedTime) {
							lastLaunchTimestamp = timestamp;
							JRNotified = false;
							if (checkParameters(false)) {
								logger.log(Level.INFO, "Spawned thread nr " + i);
								launchJobAgent(jrPid, false, i, 0);
								lastLaunchTimestamp = timestamp;
								idleTimestamp = 0;
								i ++;
							}
							else {
								monitor.sendParameter("state", "All slots busy");
								if (wholeNode == true && cpuOversubscription) {

									if (enoughMem && cpuIdle * JobAgent.MAX_CPU.longValue() >= IDLE_CPU_OVERSUBSCRIBE ) {
										logger.log(Level.INFO, "We have enough idle. Current idle = " + cpuIdle + "%");
										if (idleTimestamp == 0)
											idleTimestamp = System.currentTimeMillis()/1000;
										logger.log(Level.INFO, "In idle for " + (System.currentTimeMillis()/1000 - idleTimestamp) + " seconds.");

										if (idleTimestamp != 0 && (timestamp - idleTimestamp) >= WAIT_OVERSUBSCRIBE) {
											logger.log(Level.INFO, "Starting oversubscription workflow");
											if (checkParameters(true)) {
												int coresToOversubscribe = (int) (cpuIdle * JobAgent.MAX_CPU.longValue() / 100 - 2); //Leaving two cores for the system
												if (coresToOversubscribe > OVERSUBSCRIBED_CPU.intValue())
													coresToOversubscribe = OVERSUBSCRIBED_CPU.intValue();
												logger.log(Level.INFO, "Spawned thread nr " + i + " (Oversubscribed). Can use up to " + coresToOversubscribe + " oversubscription pool");
												JobAgent ja = launchJobAgent(jrPid, true, i, coresToOversubscribe);
												oversubscribedJobs.push(ja);
												i ++;
												idleTimestamp = 0;
												WAIT_OVERSUBSCRIBE = 240;
											}
										}
									} else if (cpuIdle * JobAgent.MAX_CPU.longValue() < IDLE_CPU_OVERSUBSCRIBE  && idleTimestamp != 0) {
										idleTimestamp = 0;
										logger.log(Level.INFO, "Reseting idle counter. CPU idle = " + cpuIdle + "%");
									}
								}

							monitor.sendParameter("statenumeric", Long.valueOf(3));
							logger.log(Level.INFO, "No new thread");
							}
						}

						long tsBeforeWait = System.currentTimeMillis();
						JobAgent.requestSync.wait(30 * 1000);
						if ( (System.currentTimeMillis() - tsBeforeWait) < 30 * 1000) { //If JR has been notified will be less than 30s
							logger.log(Level.INFO,"JR notified. Starting new JA");
							JRNotified = true;
						}
					}
					catch (final InterruptedException e) {
						logger.log(Level.WARNING, "JobRunner interrupted", e);
						return;
					}

					timestamp = System.currentTimeMillis() / 1000;

					monitor.incrementCounter("startedja");

					monitor.sendParameter("retries", Long.valueOf(JobAgent.retries.get()));

					monitor.sendParameter("remainingttl", Long.valueOf(ttlEnd - timestamp));

					if (JobAgent.retries.get() >= maxRetries && JobAgent.OVERSUBSCRIBED_CPU.longValue() == JobAgent.MAX_OVERSUBSCRIBED.longValue()) {
						JAliEnCOMMander.getInstance().q_api.getPinningInspection(new byte[JobAgent.RES_NOCPUS.intValue()], true, ConfigUtils.getLocalHostname());
						monitor.sendParameter("state", "Last JA cannot get job");
						monitor.sendParameter("statenumeric", Long.valueOf(2));
						logger.log(Level.INFO, "JobRunner going to exit from lack of jobs");
						System.exit(0);
						//break;
					}
				}
				catch (final Exception e) {
					logger.log(Level.WARNING, "JobRunner main loop caught another exception", e);
				}
			}
		}
		System.err.println("JobRunner Exiting");
		System.exit(0);
	}

	private JobAgent launchJobAgent(int jrPid, boolean oversubscribed, int i, int coresToOversubscribe) {
		JobAgent ja = new JobAgent();
		Thread jaThread = new Thread(ja, "JobAgent_" + i);
		jaThread.start();
		ja.setOversubscription(oversubscribed);
		ja.setOversubscribedAvailable(coresToOversubscribe);
		if (cpuIsolation == true && alreadyIsol == false) {
			alreadyIsol = checkAndApplyIsolation(jrPid,alreadyIsol);
		}

		monitor.sendParameter("state", "Waiting for JA to get a job");
		monitor.sendParameter("statenumeric", Long.valueOf(1));
		return ja;
	}

	/**
	 * Moves the oversubscribed job to the regular job pool
	 */
	@SuppressWarnings({ "boxing" })
	static void moveExtraJobToRegularPool() {
		JobAgent ja = oversubscribedJobs.firstElement();
		logger.log(Level.INFO, "Moving oversubscribed job to regular pool " + ja.getQueueId());
		ja.setOversubscription(false);
		OVERSUBSCRIBED_CPU += ja.getReqCPU();
		RUNNING_CPU -= ja.getReqCPU();
		logger.log(Level.INFO, "Current regular pool has now " + RUNNING_CPU + " CPU cores. Oversubscribed pool has " + OVERSUBSCRIBED_CPU + " cores");

		if (cpuIsolation == true ) {
			if (CgroupUtils.haveCgroupsv2() && CgroupUtils.hasController(ja.agentCgroupV2, "cpu") )
				ja.limitCPUcgroupsV2();
			else {
				String isolCmd = ja.addIsolation();
				NUMAExplorer.applyTaskset(isolCmd, ja.getChildPID());
				logger.log(Level.INFO, "Job " + ja.getQueueId() + " pinned to mask " + isolCmd);
			}
		}

		ja.putJobTrace("Moving agent to the regular resource pool");

		oversubscribedJobs.remove(ja);
	}

	public static void main(final String[] args) {
		ConfigUtils.setApplicationName("JobRunner");
		DispatchSSLClient.setIdleTimeout(30000);
		ConfigUtils.switchToForkProcessLaunching();
		final JobRunner jr = new JobRunner();
		jr.run();
	}

	/**
	 * Gets the JA sorter by different strategies to record in DB
	 *
	 * @param slotMem Total memory consumed in the slot
	 * @param slotMemsw
	 * @param reason
	 * @param parsedSlotLimit
	 */
	public static void recordHighestConsumer(double slotMem, double slotMemsw, String reason, double parsedSlotLimit) {
		SorterByAbsoluteMemoryUsage jobSorter1 = new SorterByAbsoluteMemoryUsage();
		sortByComparator(slotMem, slotMemsw, jobSorter1, reason, parsedSlotLimit, true);
		SorterByRelativeMemoryUsage jobSorter2 = new SorterByRelativeMemoryUsage();
		sortByComparator(slotMem, slotMemsw, jobSorter2, reason, parsedSlotLimit, false);
		SorterByTemporalGrowth jobSorter3 = new SorterByTemporalGrowth();
		sortByComparator(slotMem, slotMemsw, jobSorter3, reason, parsedSlotLimit, false);
	}

	private static void sortByComparator(double slotMem, double slotMemsw, Comparator<JobAgent> jobSorter, String reason, double parsedSlotLimit, boolean realPreemption) {
		ArrayList<JobAgent> sortedJA = new ArrayList<>(MemoryController.activeJAInstances.values());
		for (JobAgent ja : sortedJA)
			ja.checkProcessResources();
		JobAgent toPreempt = null;
		long preemptionTs = System.currentTimeMillis();
		if (!oversubscribedJobs.isEmpty()) {
			JobAgent toPreemptCandidate = oversubscribedJobs.pop();
			if (toPreemptCandidate.getQueueId() > 0)
				toPreempt = toPreemptCandidate;
		} else {
			String sorterId = jobSorter.getClass().getCanonicalName().split("\\.")[jobSorter.getClass().getCanonicalName().split("\\.").length -1];
			Collections.sort(sortedJA, jobSorter);
			if (MemoryController.debugMemoryController) {
				logger.log(Level.INFO, "Sorted jobs with " + sorterId + ": ");
				for (JobAgent ja : sortedJA)
					logger.log(Level.INFO, "Job " + ja.getQueueId() + " consuming VMEM " + ja.RES_VMEM.doubleValue() + " MB and RMEM " + ja.RES_RMEM.doubleValue() + " MB RAM");
			}
			int i = 0;
			while (i < sortedJA.size()) {
				JobAgent ja = sortedJA.get(i);
				if (ja.RES_VMEM.doubleValue() > (double) MemoryController.MIN_MEMORY_PER_CORE * ja.cpuCores / 1024) { //Here we make sure we do not kill a job that is consuming less than 2G per slot
					toPreempt = ja;
					break;
				}
				i += 1;

			}
		}

		if (toPreempt != null && !toPreempt.alreadyPreempted) {
			for (JobAgent ja : sortedJA) {
				boolean success = ja.recordPreemption(preemptionTs, slotMem, slotMemsw, ja.RES_VMEM.doubleValue(), reason, parsedSlotLimit, sortedJA.size(), toPreempt.getQueueId(),realPreemption);
				if (!success) {
					logger.log(Level.INFO, "Could not record preemption on central DB");
				}
			}
			MemoryController.preemptionRound += 1;
		}
		else if (toPreempt == null) {
			logger.log(Level.INFO, "Could not start preemption. All running jobs in the slot were consuming less than 2GB/core");
		}
	}
}
