package alien.site;

import java.util.HashMap;
import java.util.Map;

/**
 * @author costing
 * @since 2024-06-05
 */
public enum KillReason {
	/**
	 * Job finished OK
	 */
	OK(0, "Finished OK"),

	// ERROR_E codes
	/**
	 * The job wrapper JVM left a file in place as it exited due to an OOM
	 */
	OOM(10, "JW JVM out of memory"),

	/**
	 * TTL overrun
	 */
	TTL(20, "Job running for longer than the TTL"),

	/**
	 * Killed by the user, the job should not be running any more
	 */
	KILLED(30, "Job was killed"),

	/**
	 * Selected to be terminated by JAliEn to let other jobs in the slot run
	 */
	OOM_PREEMPTED(40, "Killed by JAliEn due to overuse of memory"),

	/**
	 * Long time of close to no CPU being used
	 */
	IDLE_CPU(50, "Payload idling for more than 15 minutes"),

	/**
	 * Payload has produced a core file
	 */
	CORE_DUMP(60, "Core file detected"),

	/**
	 * Too much disk space being used
	 */
	DISK_USAGE(70, "Too much disk space used"),

	/**
	 * Too much memory being used
	 */
	MEMORY_USAGE(80, "Too much memory used"),

	/**
	 * System killed a payload process due to OOM
	 */
	OOM_SYSTEM(90, "Killed by executing system due to overuse of memory"),

	// ERROR_IB codes
	/**
	 * Failed to create the payload sandbox
	 */
	SANDBOX_CREATE(201, "Cannot create the sandbox directory"),

	/**
	 * Cannot copy input files to the sandbox of the job
	 */
	INPUT_FILES(202, "Failed to download input files"),

	/**
	 * Cannot run alienv or it returns an error
	 */
	PACKAGES(203, "Cannot set up the packages environment");

	private static Map<Integer, KillReason> codeToReason = new HashMap<>();

	static {
		for (final KillReason reason : KillReason.values())
			codeToReason.put(Integer.valueOf(reason.getCode()), reason);
	}

	private final int killReason;
	private final String message;

	private KillReason(final int killReason, final String message) {
		this.killReason = killReason;
		this.message = message;
	}

	/**
	 * @return code associated to this state
	 */
	public int getCode() {
		return killReason;
	}

	/**
	 * @return user readable message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param code
	 * @return reason corresponding to the database value
	 */
	public static KillReason getReason(final int code) {
		return codeToReason.get(Integer.valueOf(code));
	}

	@Override
	public String toString() {
		return "(" + killReason + ") " + message;
	}
}
